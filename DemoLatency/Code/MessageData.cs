﻿namespace DemoLatency.Code;

public record MessageData(long Id, long Sent)
{
    public long Received { get; set; }
};