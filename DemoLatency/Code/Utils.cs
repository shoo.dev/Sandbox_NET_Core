﻿namespace DemoLatency.Code;

public static class Utils
{
    public const string ChannelForwardName = "ChannelSent";
    public const string ChannelBackName = "ChannelReceive";
}