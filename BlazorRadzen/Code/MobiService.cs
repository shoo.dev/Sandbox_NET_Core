﻿namespace BlazorRadzen.Code;

public class MobiService : BackgroundService
{
    private MobiData Data { get; }

    public MobiService(MobiData data)
    {
        Data = data;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        using var timer = new PeriodicTimer(TimeSpan.FromMilliseconds(16));

        var a = 0;
        while (await timer.WaitForNextTickAsync(stoppingToken))
        {
            stoppingToken.ThrowIfCancellationRequested();
            
            Data.Active = Math.Sin(Math.PI * a / 180.0) * 10;
            Data.Update = Math.Cos(Math.PI * a / 180.0) * 10;
            
            a++;
        }
    }
}